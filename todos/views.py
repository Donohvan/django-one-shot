from django.shortcuts import get_object_or_404, render
from todos.models import TodoList

# Create your views here.


def todos_list(request):
    todos = TodoList.objects.all()
    context = {
        "todos_list": todos,
    }
    return render(request, "todos/list.html", context)


def todo_list_detail(request, id):
    detail = get_object_or_404(TodoList, id=id)
    context = {
        "todo_list_detail": detail,
    }
    return render(request, "todos/detail.html", context)
