from django.urls import path
from todos.views import todos_list, todo_list_detail

urlpatterns = [
    path("<int:id>/", todo_list_detail, name="todo_list_detail"),
    path("", todos_list, name="todos_list"),
]
